# CRM initializer

This project contains two applications one is  **CSV reader** in charge of loading some customers data from a CSV file and send them to the other applcation involved **CRM Integrator**.

1. [Overview](#overview)
2. [Up and Running](#upandrunning)
3. [Developer notes](#developer-notes)
4. [Configuration](#configuration)
5. [Roadmap](#roadmap)

<a id="overview"></a>

# Overview

A high level idea of the project is exposed at this image:

![](doc/crm-with-concurrency.png)

There are two applications involved:

- **CSV Reader** is the app in charge of loading the customers from the CSV to the database and send them to the **CRM integrator** through an HTTP interface. This application involves two separated process:

    - **Load Customers from CSV to DB**:

        -  (1) Read customers on batches of 100 to avoid memory overloading.

        -  (2) Save customers in batches of 100 because is faster than one by one.

        -  (2) One job is generated per each customer. Each job is saved in a table that will work like a litle queue system. Each job contains the `id` of the customer and a boolean called `in_proccess` to indicate if the customers has already been sent.

    -  **Publish customers to the CRM Integrator**:

        - (3) The next customers to be sent will be selected based on the jobs table. The selected jobs will change their state `in_process: true`.

        - (4) **Publish customers to CRM Integrator**. The customers will be sent to the CRM integrator. The response will be checked in order to detect which customers have been correctly saved on the CRM integrator and which ones not. If a customer has been correctly saved on the CRM integrator, his job will be deleted from the database, if it has failed it will be set back his job state from `in_process: true` to `in_process: false`.


This process of publishing customers will continue, keeping alive the **CSV Reader** application. If in one iteraction there is no customer to be sent the process will sleep for a second.

 - **CRM Integrator**. On any incoming batch of customers it will:

     -  It will decide  which customer will be correctly saved and which one not. **This success/fail ratio has to be set using the environment variable *SUCCESS_PERCENTAGE***. This variable is mandatory to be set. This is defined at the [crm_integrator/core](/crm_integrator/internal/core/core.go) package.

    - It will check how many customers are still pending. This is logged so we can observerd how it decreases along the time.

At this image we can see how on each batch it is checked how many customers are still pending:

![](doc/output.png)

## Other ideas that worths to be mentioned:

- It was decided to not use goroutines or any other more complex design to parallelize the exporting/publishing feature. I understantd it is useful to have several goroutines in order to share the CPU load of processing the jobs but in our case there was almost no CPU bound tasks. It is preferrable to send one HTTP request with several customers on it than various HTTP post with only one customer. Since also **CRM integrator** was also up to my design I decided to do it that way.

- **CSV reader** counts with an [interface](csv_reader/internal/db/db) to connnect with the database. This decouples the source of the data and allows to use one [Stub](csv_reader/internal/db/stub), avoding to have to connect to the database during the tests, [like on these tests](csv_reader/internal/export/export_test.go#L19) for export package.

- **CSV Reader**: Similarly to latest point, it has been provided an [interface](csv_reader/internal/publisher/publisher.go) and a [stub](csv_reader/internal/publisher/stub.go) for the the publisher, so we can evaluate the behaviour of the [export](csv_reader/internal/export/export.go) package without having to connect to db or the http publisher.

- The project is not fully tested. There are some tests mainly done as help during the development process but not with objective of getting a 100% coverage. In fact no test has been written on the CRM Integrator.

<a id="upandrunning"></a>

## Up and Running

### With docker:

It is mandatory to indicate the environment variable *SUCCESS_PERCENTAGE*.

```
SUCCESS_PERCENTAGE=10 docker-compose up
```
Using this command you will have three running containers:

1. CSV Reader.

2.  CRM Integrator.

3.  Postgres DB.

The dockers will be pulled from the registry. It also possible to build them locally just using the `build` staments instead of the `image` in the [docker-compose.yml](docker-compose.yml) file.

### With Local binaries

The first thing we need is the **Postgres DB**. We will use docker to provide us it:

``` Bash
# ./
docker-compose up postgres
```

Let's run the **CRM integrator**:

```Bash
# ./
cd crm_integrator
SUCCESS_PERCENTAGE=5 go run cmd/main.go
```

Let's run the **CSV reader**
```Bash
# ./
cd csv_reader
go run cmd/main.go
```

## Developer notes


### Tests

Only have been added tests for the CSV_Reader, so we will run this tests:

Run postgres db for testing:

``` Bash
# ./
cd csv_reader
docker-compose -f docker-compose.test.yml up
```

Execute tests:

``` Bash
# ./csv_reader
go test ./...
```

In some cases we can have troubles with a previously created postgres docker due to that the tables are already presents. In that cases we can use this command to clean the docker:
```
docker ps -a | grep posrg | awk '{print $1}' | xargs docker rm -f
```

#### CSV reader

```
cd csv_reader
docker build -t csv_reader
```

Run docker exposing api port:
``` Bash
docker run -d  csv_reader
```


Build & Push to registry
```
cd csv_reader
docker build -t registry.gitlab.com/jkmrto/crm_initializer/csv_reader .
docker push registry.gitlab.com/jkmrto/crm_initializer/csv_reader
```

#### CRM integrator

Build CRM integrator docker:

```Bash
# ./
cd crm_integrator
docker build -t crm_integrator
```

Run docker exposing api port:
``` Bash
docker run -d -p  5010:5010 -e SUCCESS_PERCENTAGE=5 crm_integrator
```

Build & Push to registry
```Bash
# ./
cd crm_integrator
docker build -t registry.gitlab.com/jkmrto/crm_initializer/crm_integrator .
docker push registry.gitlab.com/jkmrto/crm_initializer/crm_integrator
```

We can check that the CRM intrator is receiving the messages using this curl request:

```Bash
curl -X POST \
  http://localhost:5010/crm_integrator/customer_list/new \
  -H 'Content-Type: application/json' \
  -d '[
      {
        "id": 1,
        "first": "Peter",
        "lastName": "la anguila",
        "email": "peter@la-anquila",
        "phone": "42"
    },
]'
```
### Gitlab CI

This project includes Gitlab CI pipeline. The main utility for this project is for running the tests and check the test coverage. Other stages can be added like:

- Building the docker and pushing to a registry.

- Deploy the application to a Test/Prod server.

- Build and Deploy the documentation.


Sometimes the pipeline can start to fail so it is needed to be debuged. In these cases we will have to try to replicate the behaviour locally using the same docker that in the pipeline.

```Bash
docker run  -v ${PWD}:/code -it --entrypoint=bash golang:1.12-stretch
```

<a id="configuration"></a>
# Configuration


We are able to load different configurations at runtime.
This are the files where the configuration is indicated:

- development configuration: ./config/dev.json

- test configuration:./config/test.json

- prod configuration: ./config/prod.json

We just need to indicate the ENV variable to indicate the configuration to load. THis project also allows configuration via envirionment variables. They configurables paramenters can be found at the [config package](csv_reader/internal/config/config.go).

<a id="roadmap"></a>
# Roadmap

THis roadmap is mainly for personal information to keep recort of the state of the project.

- [x] Init project. (Csv reader spike)

- [x] Postgres db in docker with migrations at entrypoint.

    - [x] Customers table migrations

- [x] (CSV_reader) Define CSV reader package

    - [x] Load CSV in batches.

    - [ ] Avoid reading the first row.

    - [x] Verbose output when the process is complete.

- [ ] (CSV_reader) Define DB package.

    - [x] Initialize db connection by configuration.

    - [x] Unit test for the query (InsertCustomers().

    - [x] Insert data to DB in batches.

    - [x] Create database stub.

    - [x] Use interface to decouple db (Stub[tests] or Postgres[dev/prod])

    - [x] Implement RemoveCustomerJobs().

    - [x] Tests for RemoveCustomerJobs().


- [x] (CSV Reader) Define model package

    - [x] Customer model.

- [x] (CSV Reader) Api Rest interface

    - [x] (CSV reader) Build Http post interface.

    - [x] (CSV reader) Create Http stub to enable testing.

    - [x] Tests??

- [x] (CRM integrator) Build Http client on CRM integrator.

    - [x] Init Server with gorilla/mux

    - [x] Accept posts, json enconde at /crm_integrator/customer/new

    - [x] Respond ssuccesfully for all customers as json.

    - [x] Randonize failures.

    - [x] Evaluate if all the customers have arrived someway.

- [ ] (CSV reader) DB queue system to send information to CRM integrator.

    - [x] Evaluate & best solution.

        - [x] New table "customers_queue" to mark the customer correctly sent.

        - [x] Populate Customers Job Queue After users at inserted in each batch.

            - [x]  (Refactor) Move BuildNewJobsByCustomers logic out of db package.

        - [x] Check customers queue and send them in batchs.

            - [x] Build NextCustomersToSend() on db package.

            - [x] Build UpdateStateOnCustomersJobs on db package.

            - [x] Create Exporter pacakge:

            - [ ] Create Tests for Exporter pacakge to handle this.

                - [x] Unit test for exporter.Export() (OneCustomer)

                - [ ] Unit test for exporter.Export() (ThreeCustomer)

                - [ ] Unit Tests for exporter.upateCustomerJobsStatus()

                - [ ] Unit Tests for exporter.splitJobsByResult()

                - [ ] Unit Test when fail on the publish.

            - [x] Send customers via HTTP interface. (http_publisher? package)

            - [x] Check answer and update customersJobs state.

            - [ ] Paralelize publishing process??, is worthy?

        - [ ] Tests for dbHandler.InsertCustomersOnQueue()

        - [ ] Test for postgres buildQueryInsertCustomers()

    - [ ] Add is_available "endpoint" on CRM integrator.

    - [ ] Check is_available "endpoint" on CSV Reader before sending data.

- [x] Configuration

    - [x] Read configuration from file.

    - [x] Read configuration from file in db tests.

    - [x] Configuration based on Env variables.

- [x] Dockerize.

    - [x] Docker for CRM integrator.

    - [x] Docker for CSV reader.

    - [x] Docker compose for both application.

    - [x] Second stage on dockerfile for CRM integrator to reduce size

    - [x] Second stage on dockerfile for CSV reader to reduce size

- [x] Readme

    - [x] Create architecture diagram.

    - [x] Overview section.

    - [x] Up and Running Section.


- [x] Gitlab CI

    - [x] Test in gitlab CI

    - [x] Coverage badge

    - [x] Pipeline badge

    - [x] Pipeline will keep failing until configuration by ENV by ready

    - [x] Build & Push csv_reader docker on pipeline

    - [x] Build & Push crm_integrator docker on pipeline

- [ ] Loggin system

    - [ ] There should be some ways the way the logging is done.


# Who do I talk to?
For any question ask to jkmrto@gmail.com