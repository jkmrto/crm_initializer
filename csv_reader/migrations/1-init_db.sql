CREATE TABLE customers (
  id SERIAL PRIMARY KEY,
  first_name varchar(40),
  last_name varchar(40),
  email varchar(40),
  phone varchar(20)
);