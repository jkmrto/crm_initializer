CREATE TABLE customers_jobs (
    customer_id BIGINT PRIMARY KEY,
    in_process boolean,
    FOREIGN KEY(customer_id) REFERENCES customers(id)
);