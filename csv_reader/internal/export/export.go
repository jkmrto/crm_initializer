package export

import (
	"log"
	"time"

	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/db"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/publisher"
)

var batchesExporter = 10

// Export runs everything
func Export(dbHandler db.Db, publisher publisher.Publisher, quit <-chan interface{}) {

	for {

		// Get Next Customers to be sent
		customers, err := dbHandler.ReadNextCustomersJob(batchesExporter)
		if err != nil {
			log.Printf("[Warn] Error reading next customers to export: %+v", err)
			continue
		}

		// Check if there is any pending customer
		// YES: Update job state at DB, send customers on publisher, update job state based on publisher results.
		// NO:  Sleep for a second and continue the loop.
		if len(customers) == 0 {
			log.Printf("No customers to send. Let's sleep for a second")
			time.Sleep(time.Second)
			continue
		} else {
			jobsResult, err := doExportCustomers(dbHandler, publisher, customers)
			if err != nil {
				continue
			}
			upateCustomerJobsStatus(dbHandler, jobsResult)
		}

		// Check Quit Channel and Exit when signal arrived
		select {
		case <-quit:
			log.Printf("Quite Message Received. Export exiting")
			return
		default:
			continue
		}
	}
}

func doExportCustomers(dbHandler db.Db, publisher publisher.Publisher, customers []model.Customer) (jobsToResult map[int]string, err error) {
	customersIds := model.GetIDs(customers)
	err = dbHandler.UpdateCustomerJobsInProcess(customersIds, true)
	if err != nil {
		log.Printf("[Warn] Error setting customer jobs in process: %+v", err)
		return jobsToResult, err
	}

	jobsToResult, err = publisher.Publish(customers)
	if err != nil {
		log.Printf("[Exporter] Error on publisher: %+v", err)
		jobsToResult = buildJobResultsAsFail(model.GetIDs(customers))
	}

	return
}

func upateCustomerJobsStatus(dbHandler db.Db, jobsResult map[int]string) error {
	successIds, failIds := splitJobsByResult(jobsResult)

	// TODO: Implement retrier in case remove jobs fails
	if len(successIds) > 0 {
		err := dbHandler.RemoveCustomerJobs(successIds)
		if err != nil {
			log.Printf("[Error] Error removing success jobs: %+v", err)
			return err
		}
	}

	// TODO: Implement retrier in case update jobs fails
	if len(failIds) > 0 {
		err := dbHandler.UpdateCustomerJobsInProcess(failIds, false)
		if err != nil {
			log.Printf("[Error] Error updating jobs status: %+v", err)
			return err
		}
	}
	return nil
}

func splitJobsByResult(jobsResult map[int]string) ([]int, []int) {
	successIds := []int{}
	failIds := []int{}
	for customerID, jobStatus := range jobsResult {
		switch jobStatus {
		case "success":
			successIds = append(successIds, customerID)
		case "fail":
			failIds = append(failIds, customerID)
		default:
			log.Printf("[Warn] Unexpected job status: %s", jobStatus)
		}
	}
	return successIds, failIds
}

func buildJobResultsAsFail(ids []int) map[int]string {
	jobsToResult := make(map[int]string)
	for _, id := range ids {
		jobsToResult[id] = "fail"
	}
	return jobsToResult
}
