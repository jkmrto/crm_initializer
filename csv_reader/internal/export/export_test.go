package export

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/db"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/publisher"
)

var dummyCustommer = model.Customer{
	ID:        1,
	FirstName: "Juan Carlos",
	LastName:  "Martinez",
	Email:     "juan@gmail.com",
	Phone:     "840 586 9744",
}

func TestExporterOneCustomer(t *testing.T) {
	customers := []model.Customer{dummyCustommer}

	var dbHandler = &db.Stub{}

	dbHandler.InsertCustomers(customers)
	customersJobs := model.BuildNewJobsByCustomers(customers)
	dbHandler.InsertCustomersJobsOnQueue(customersJobs)

	quit := make(chan interface{})
	var publisher = &publisher.Stub{}
	go Export(dbHandler, publisher, quit)

	// Wait a second to get the customer exported
	time.Sleep(100 * time.Millisecond)

	// Quit export goroutine
	close(quit)

	describe := "When only one customer on db: "
	assert.Equal(t, 1, len(publisher.PublishHistory), describe+"Only one batch must be sent")
	assert.Equal(t, dummyCustommer, publisher.PublishHistory[0][0], describe+"The first batch only send customer1")
}

func TestSplitJobsByResult(t *testing.T) {
	successIds, failIds := splitJobsByResult(map[int]string{
		1: "success",
		2: "fail",
	})

	describe := "When calling SplitJobsByResult({1: success, 2: fail}): "
	assert.Equal(t, 1, successIds[0], describe+"successIds is [1]")
	assert.Equal(t, 2, failIds[0], describe+"failIds is [2]")
}
