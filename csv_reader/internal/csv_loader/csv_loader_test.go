package csv_loader

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

func TestParseCustomerValid(t *testing.T) {
	customer, err := parseCustomer([]string{"1", "Hadleigh", "Mahedy", "hmahedy0@diigo.com", "840 586 9744"})

	expectedCustomer := model.Customer{
		ID:        1,
		FirstName: "Hadleigh",
		LastName:  "Mahedy",
		Email:     "hmahedy0@diigo.com",
		Phone:     "840 586 9744",
	}

	assert.Equal(t, err, nil, "CSV row is correctly parsed to customer")
	assert.Equal(t, customer, expectedCustomer, "CSV row is correctly parsed to customer")
}

func TestParseCustomerInvalid(t *testing.T) {
	customer, err := parseCustomer([]string{"no-valid", "Hadleigh", "Mahedy", "hmahedy0@diigo.com", "840 586 9744"})
	assert_msg := "CSV row is wrongly to be parsed"
	assert.NotEqual(t, err, nil, assert_msg)
	assert.Equal(t, customer, model.Customer{}, assert_msg)
}
