package csv_loader

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"
	"strconv"

	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/db"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

// LoadToDB loads CSV file given by `file` to `dbHandler`
func LoadToDB(file string, dbHandler db.Db) {
	batchSize := 100
	csvFile, _ := os.Open(file)
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var customerBatch []model.Customer
	for {
		line, error := reader.Read()
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}

		customer, err := parseCustomer(line)
		if err != nil {
			log.Printf("[Warn] Error: %+v ,parsing %+v", error, line)
			continue
		}

		customerBatch = append(customerBatch, customer)
		customerBatch = insertBatch(dbHandler, customerBatch, batchSize)
	}

	log.Printf("CSV reading Complete\n")
}

func insertBatch(dbHandler db.Db, batchCustomer []model.Customer, sizeToUnload int) []model.Customer {
	if len(batchCustomer) == sizeToUnload {
		err := dbHandler.InsertCustomers(batchCustomer)
		if err != nil {
			log.Printf("[WARN] Error inserting customers batch: %+v", err)
		} else {
			insertJobsBatch(dbHandler, batchCustomer)
		}
		return []model.Customer{}
	}

	return batchCustomer
}

func insertJobsBatch(dbHandler db.Db, batchCustomer []model.Customer) {
	customerJobs := model.BuildNewJobsByCustomers(batchCustomer)
	err := dbHandler.InsertCustomersJobsOnQueue(customerJobs)
	if err != nil {
		log.Printf("[WARN] Error inserting customers jobs: %+v", err)
	} else {
		log.Printf("Batch correctly inserted\n")
	}
}

func parseCustomer(line []string) (customer model.Customer, err error) {
	id, err := strconv.Atoi(line[0])
	if err != nil {
		return model.Customer{}, err
	}

	customer = model.BuildCustomer(id, line[1], line[2], line[3], line[4])
	return customer, nil
}
