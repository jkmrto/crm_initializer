package publisher

import (
	"fmt"

	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

type Stub struct {
	PublishHistory [][]model.Customer
}

func (self *Stub) Publish(customers []model.Customer) (map[int]string, error) {
	fmt.Printf("[Publisher Stub]: publish: %+v\n", customers)
	self.PublishHistory = append(self.PublishHistory, customers)

	response := make(map[int]string)
	for _, customer := range customers {
		response[customer.ID] = "success"
	}

	return response, nil
}

func (self *Stub) CheckStatus() {
	fmt.Printf("\nPublisherHistory: %+v", self.PublishHistory)
}
