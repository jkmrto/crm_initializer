package publisher

import (
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

type Publisher interface {
	Publish(customers []model.Customer) (map[int]string, error)
}
