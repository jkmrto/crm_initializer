package publisher

import (
	"bytes"
	"encoding/json"
	"io"
	"net/http"

	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

// CustomerJobResult defines the model for the jobs table
type CustomerJobResult struct {
	CustomerID int    `json:"customerId"`
	Result     string `json:"result"`
}

type httpPublisher struct {
	crmIntegratorURL string
	client           *http.Client
}

// NewHTTPPublisher createsa new HttpPublisher
func NewHTTPublisher(config map[string]string) *httpPublisher {
	return &httpPublisher{
		client:           &http.Client{},
		crmIntegratorURL: buildCRMIntegratorURL(config),
	}
}

func (pub *httpPublisher) Publish(customers []model.Customer) (map[int]string, error) {
	req := pub.buildRequest(customers)
	resp, err := pub.client.Do(req)

	if err != nil {
		return make(map[int]string), err
	}

	jobsResult := decodeBody(resp.Body)
	resp.Body.Close()

	// TODO: This should be moved to the exporter in order to leave the publisher
	// with the less amount of logic possible
	jobsToResult := buildJobsToResult(jobsResult)

	return jobsToResult, err
}

func (pub httpPublisher) buildRequest(customers []model.Customer) *http.Request {
	url := pub.crmIntegratorURL + "/crm_integrator/customer_list/new"
	customersEncoded, _ := json.Marshal(customers)
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(customersEncoded))
	req.Header.Set("Content-Type", "application/json")
	return req
}

func buildCRMIntegratorURL(config map[string]string) string {
	return "http://" + config["host"] + ":" + config["port"]
}

func decodeBody(body io.Reader) []CustomerJobResult {
	var customerJobsResult []CustomerJobResult
	decoder := json.NewDecoder(body)
	decoder.Decode(&customerJobsResult)
	return customerJobsResult
}

func buildJobsToResult(customerJobsResult []CustomerJobResult) map[int]string {
	jobsToResult := make(map[int]string)
	for _, jobResult := range customerJobsResult {
		jobsToResult[jobResult.CustomerID] = jobResult.Result
	}
	return jobsToResult
}
