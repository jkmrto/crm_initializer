package config

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/tkanos/gonfig"
)

// Configuration keeps the configuration vars defined at startup
type Configuration struct {
	PostgresUser      string `env:"POSTGRES_USER"`
	PostgresPwd       string `env:"POSTGRES_PASSWORD"`
	PostgresHost      string `env:"POSTGRES_HOST"`
	PostgresPort      int    `env:"POSTGRES_PORT"`
	PostgresDb        string `env:"POSTGRES_DB"`
	CRMIntegratorHost string `env:"CRM_INTEGRATOR_HOST"`
	CRMIntegratorPort int    `env:"CRM_INTEGRATOR_PORT"`
	CustomersCSV      string
}

// LoadConfig fucks
func LoadConfig(configPath string) Configuration {
	conf := Configuration{}
	err := gonfig.GetConf(configPath, &conf)
	if err != nil {
		panic(err)
	}

	return conf
}

// LoadPostgresConfig loads postgres configuration from app config.
// TODO: Should PostgresConfig be an struct instead than a map ??
func LoadPostgresConfig(configPath string) map[string]string {
	return GetPostgresConfig(LoadConfig(configPath))
}

// GetPostgresConfig gets postgres configuration
func GetPostgresConfig(conf Configuration) map[string]string {
	log.Printf("[Info] Configuration: %+v", conf)
	return map[string]string{
		"user":   conf.PostgresUser,
		"pwd":    conf.PostgresPwd,
		"dbname": conf.PostgresDb,
		"host":   conf.PostgresHost,
		"port":   strconv.Itoa(conf.PostgresPort),
	}
}

// GetHTTPPublisherConfig gets Http publisher configuration
func GetHTTPPublisherConfig(conf Configuration) map[string]string {
	log.Printf("[Info] Configuration: %+v", conf)
	return map[string]string{
		"host": conf.CRMIntegratorHost,
		"port": strconv.Itoa(conf.CRMIntegratorPort),
	}
}

// LoadAppConfig from $ENV.json
// In case of not setled ENV variable, 'dev' will be used
func LoadAppConfig() Configuration {
	env := os.Getenv("ENV")
	if env == "" {
		env = "dev"
	}

	configPath := fmt.Sprintf("config/%s.json", env)
	return LoadConfig(configPath)

}
