package model

// Customer represents each customer. Also the model customer for the db
type Customer struct {
	ID        int
	FirstName string
	LastName  string
	Email     string
	Phone     string
}

// BuildCustomer allows to create a new customer.
func BuildCustomer(id int, firstName string, lastName string, email string, phone string) Customer {
	return Customer{
		ID:        id,
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Phone:     phone,
	}
}

// GetIDs allows to gets a list of the IDs of the given 'customers'.
func GetIDs(customers []Customer) []int {
	ids := []int{}
	for _, customer := range customers {
		ids = append(ids, customer.ID)
	}
	return ids
}

// CustomerJob ...
type CustomerJob struct {
	CustomerID int
	InProcess  bool
}

// NewCustomerJob ...
func NewCustomerJob(id int) CustomerJob {
	return CustomerJob{
		CustomerID: id,
		InProcess:  false,
	}
}

// BuildNewJobsByCustomers ...
func BuildNewJobsByCustomers(customers []Customer) []CustomerJob {
	customersJobs := []CustomerJob{}

	for _, customer := range customers {
		customerJob := NewCustomerJob(customer.ID)
		customersJobs = append(customersJobs, customerJob)
	}

	return customersJobs
}
