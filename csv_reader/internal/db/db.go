package db

import (
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/db/postgres"
)

type Db interface {
	// ReadCustomerById(int) (model.Customer, error)
	InsertCustomers([]model.Customer) error
	InsertCustomersJobsOnQueue([]model.CustomerJob) error
	ReadNextCustomersJob(int) ([]model.Customer, error)
	UpdateCustomerJobsInProcess([]int, bool) error
	RemoveCustomerJobs([]int) error
}

func StartPostgresConnection(dbSettings map[string]string) Db{
	return postgres.StartConnection(dbSettings)
}