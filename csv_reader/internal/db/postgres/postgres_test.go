package postgres

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/config"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

var dummyCustomer = model.Customer{
	ID:        1,
	FirstName: "Juan Carlos",
	LastName:  "Martinez",
	Email:     "juan@gmail.com",
	Phone:     "840 586 9744",
}

var dbHandler *Postgres
var testConfigPath = "../../../config/test.json"

func init() {
	dbSettings := config.LoadPostgresConfig(testConfigPath)
	fmt.Printf("This is the test configuration: %+v", dbSettings)
	dbHandler = StartConnection(dbSettings)
}

func TestBuildConnectionString(t *testing.T) {
	dbConfig := map[string]string{
		"user":   "user",
		"pwd":    "pwd",
		"dbname": "dbname",
		"host":   "host",
		"port":   "5432",
	}
	got := buildConnectionString(dbConfig)
	expected := "postgres://user:pwd@host:5432/dbname?sslmode=disable"
	assert.Equal(t, got, expected, "Connection String is correctly built")

}

func TestBuildQueryInsertCustomers(t *testing.T) {
	query := buildQueryInsertCustomers([]model.Customer{dummyCustomer, dummyCustomer})
	expectedQuery := "INSERT INTO customers(id, first_name, last_name, email, phone) VALUES (1, 'Juan Carlos', 'Martinez', 'juan@gmail.com', '840 586 9744'), (1, 'Juan Carlos', 'Martinez', 'juan@gmail.com', '840 586 9744');"
	assert.Equal(t, query, expectedQuery, "InsertCustomers query is successfully created")
}

func TestFormat(t *testing.T) {
	assert.Equal(t, format("O'Day"), "O''Day", "String with ' needs to be replicated to ''")
}

func TestInsertCustomers(t *testing.T) {
	dbHandler.CleanCustomersTable()
	dbHandler.InsertCustomers([]model.Customer{dummyCustomer})
	customerRead, _ := dbHandler.ReadCustomerByID(1)

	assert.Equal(t, customerRead, dummyCustomer, "A customer has been correctly insterted and read")
}

func TestReadNextCustomersJob(t *testing.T) {
	customer1 := dummyCustomer
	customer2 := dummyCustomer
	customer2.ID = 2
	customer3 := dummyCustomer
	customer3.ID = 3
	customers := []model.Customer{customer1, customer2, customer3}

	dbHandler.CleanCustomersTable()
	dbHandler.CleanCustomersJobsQueue()

	dbHandler.InsertCustomers(customers)
	customerJobs := model.BuildNewJobsByCustomers(customers)
	err := dbHandler.InsertCustomersJobsOnQueue(customerJobs)

	nextCustomers, err := dbHandler.ReadNextCustomersJob(2)

	assert.Equal(t, err, nil, "Error getting customers to send from db")
	assert.Equal(t, 2, len(nextCustomers), "Two customers must be returned")

}

func TestUpdateCustomerJobsInProcess(t *testing.T) {
	customer1 := dummyCustomer
	customer2 := dummyCustomer
	customer2.ID = 2
	customer3 := dummyCustomer
	customer3.ID = 3

	dbHandler.CleanDb()
	dbHandler.InsertCustomers([]model.Customer{customer1, customer2, customer3})

	customerJobs := model.BuildNewJobsByCustomers([]model.Customer{customer1, customer2, customer3})
	dbHandler.InsertCustomersJobsOnQueue(customerJobs)

	ids := model.GetIDs([]model.Customer{customer2, customer3})
	dbHandler.UpdateCustomerJobsInProcess(ids, true) // This jobs are already in process

	customers, err := dbHandler.ReadNextCustomersJob(2)

	assert.Equal(t, err, nil, "Error getting next customers jobs")
	assert.Equal(t, 1, len(customers), "Only one customer job is still peding")
	assert.Equal(t, 1, customers[0].ID, "The pending customer_id is 1")

}

func TestRemoveCustomerJobs(t *testing.T) {
	dbHandler.CleanDb()

	customer1 := dummyCustomer
	customer2 := dummyCustomer
	customer2.ID = 2
	customer3 := dummyCustomer
	customer3.ID = 3
	customers := []model.Customer{customer1, customer2, customer3}

	// Init customers and jobs
	dbHandler.InsertCustomers(customers)
	customerJobs := model.BuildNewJobsByCustomers(customers)
	dbHandler.InsertCustomersJobsOnQueue(customerJobs)

	dbHandler.RemoveCustomerJobs([]int{1, 3})
	stayingCustomersJob, err := dbHandler.ReadCustomersJob()

	fmt.Printf("\nstayingCustomersJob: %+v", stayingCustomersJob)

	describe := "When having 3 customers, and later removing the 1rd and the 3rd from the queue: "
	assert.Equal(t, err, nil, describe+"No error should happen.")
	assert.Equal(t, 1, len(stayingCustomersJob), describe+"Only one customer job is still peding.")
	assert.Equal(t, 2, stayingCustomersJob[0].CustomerID, describe+"The pending customer job is 2.")

}
