package postgres

import (
	"database/sql"
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	_ "github.com/lib/pq"
)

var max_connection_attempts = 5

type Postgres struct {
	conn *sql.DB
}

func StartConnection(dbSettings map[string]string) *Postgres {
	conn := connect(buildConnectionString(dbSettings), 1)
	return &Postgres{conn: conn}
}

func (p *Postgres) Close(db *sql.DB) {
	db.Close()
}

func (p *Postgres) CleanDb() {
	p.CleanCustomersTable()
	p.CleanCustomersJobsQueue()
}

func buildConnectionString(dbSettings map[string]string) string {
	connectionString := fmt.Sprintf("postgres://%s:%s@%s:%s/%s?sslmode=disable",
		dbSettings["user"], dbSettings["pwd"], dbSettings["host"], dbSettings["port"], dbSettings["dbname"])
	return connectionString
}

func connect(connectionStr string, attempt int) *sql.DB {
	conn, _ := sql.Open("postgres", connectionStr)
	err := conn.Ping()
	if err != nil {
		log.Printf("[WARN] Error attempting to connect to db: %+v", err)
		reconnect(connectionStr, attempt+1)
	}
	return conn
}

func reconnect(connectionStr string, attempt int) *sql.DB {
	if attempt <= max_connection_attempts {
		log.Printf("[INFO] Retrying to connect to db in 1 seconds, attempt: %d", attempt)
		time.Sleep(time.Second)
		return connect(connectionStr, attempt)
	} else {
		log.Panicf("[ERROR] Maximum number of conection attempts achieved: %d", max_connection_attempts)
	}
	return nil
}

func format(str string) string {
	return strings.ReplaceAll(str, "'", "''")
}

func buildSqlIntegerList(ids []int) string {
	var idsSring []string
	for _, id := range ids {
		idsSring = append(idsSring, strconv.Itoa(id))
	}

	return "(" + strings.Join(idsSring, ", ") + ")"
}
