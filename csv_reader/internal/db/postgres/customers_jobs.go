package postgres

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

// ReadCustomersJob ...
func (p *Postgres) ReadCustomersJob() ([]model.CustomerJob, error) {
	query := (`SELECT * FROM customers_jobs`)

	rows, err := p.conn.Query(query)
	if err != nil {
		return []model.CustomerJob{}, err
	}
	defer rows.Close()

	var customerJobs []model.CustomerJob
	for rows.Next() {
		var customerJob model.CustomerJob
		err := rows.Scan(&customerJob.CustomerID, &customerJob.InProcess)
		if err != nil {
			log.Printf("[Warn] Error getting customer from db %+v", err)
		} else {
			customerJobs = append(customerJobs, customerJob)
		}
	}

	return customerJobs, nil
}

// InsertCustomersJobsOnQueue ...
func (p *Postgres) InsertCustomersJobsOnQueue(customerJobs []model.CustomerJob) error {
	query := buildQueryInsertCustomersOnJobsQueue(customerJobs)
	_, err := p.conn.Query(query)
	return err
}

// CleanCustomersJobsQueue ...
func (p *Postgres) CleanCustomersJobsQueue() error {
	_, err := p.conn.Query("DELETE FROM customers_jobs")
	return err
}

// ReadNextCustomersJob ...
func (p Postgres) ReadNextCustomersJob(amount int) ([]model.Customer, error) {
	query := fmt.Sprintf(`SELECT customers.*
		FROM customers_jobs
		JOIN customers ON customers.id = customers_jobs.customer_id
	    WHERE customers_jobs.in_process=false
		ORDER BY customers.id asc
		Limit %d`, amount)

	rows, err := p.conn.Query(query)
	if err != nil {
		return []model.Customer{}, err
	}
	defer rows.Close()

	var customers []model.Customer
	for rows.Next() {
		var customer model.Customer
		err := rows.Scan(&customer.ID, &customer.FirstName, &customer.LastName, &customer.Email, &customer.Phone)
		if err != nil {
			log.Printf("[Warn] Error getting customer from db %+v", err)
		} else {
			customers = append(customers, customer)
		}
	}

	return customers, nil
}

// UpdateCustomerJobsInProcess ...
func (p *Postgres) UpdateCustomerJobsInProcess(ids []int, status bool) error {
	values := buildSqlIntegerList(ids)
	query := fmt.Sprintf(`UPDATE customers_jobs
		SET in_process = %t
		WHERE customer_id in %s;`, status, values)

	_, err := p.conn.Exec(query)

	return err
}

// RemoveCustomerJobs ...
func (p *Postgres) RemoveCustomerJobs(customerIds []int) error {
	values := buildSqlIntegerList(customerIds)
	query := fmt.Sprintf(`DELETE FROM customers_jobs WHERE customer_id in %s`, values)
	_, err := p.conn.Exec(query)
	return err
}

func buildQueryInsertCustomersOnJobsQueue(customerJobs []model.CustomerJob) string {
	head := "INSERT INTO customers_jobs(customer_id, in_process) VALUES"

	customerJobValues := []string{}
	for _, customerJob := range customerJobs {
		customerJobValue := fmt.Sprintf("(%d, '%t')", customerJob.CustomerID, customerJob.InProcess)
		customerJobValues = append(customerJobValues, customerJobValue)
	}

	return head + " " + strings.Join(customerJobValues[:], ", ") + ";"
}
