package postgres

import (
	"fmt"
	"strings"

	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

func (p *Postgres) InsertCustomers(customers []model.Customer) error {
	_, err := p.conn.Query(buildQueryInsertCustomers(customers))
	return err
}

func (p Postgres) ReadCustomerByID(id int) (model.Customer, error) {
	row := p.conn.QueryRow("SELECT * from customers WHERE id=$1", id)

	var customer model.Customer
	err := row.Scan(&customer.ID, &customer.FirstName, &customer.LastName,
		&customer.Email, &customer.Phone)

	return customer, err
}

// CleanCustomersTable allows to clean the 'customer' table
func (p *Postgres) CleanCustomersTable() error {
	_, err := p.conn.Query("DELETE FROM customers")
	return err
}

// This is not the best way to build the query since we are exposed to sql injection.
// Anyway the data is read from a CSV not from a client form.
func buildQueryInsertCustomers(customers []model.Customer) string {
	head := "INSERT INTO customers(id, first_name, last_name, email, phone) VALUES"

	customersValues := []string{}
	for _, customer := range customers {
		customerValue := fmt.Sprintf("(%d, '%s', '%s', '%s', '%s')",
			customer.ID, format(customer.FirstName), format(customer.LastName), format(customer.Email), format(customer.Phone))
		customersValues = append(customersValues, customerValue)
	}

	return head + " " + strings.Join(customersValues[:], ", ") + ";"
}
