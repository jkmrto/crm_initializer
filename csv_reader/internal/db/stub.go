package db

import (
	"fmt"

	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/model"
)

// Stub of the Database for testing purposes
type Stub struct {
	Customers    []model.Customer
	CustomersJob []model.CustomerJob
}

// InsertCustomers ...
func (stub *Stub) InsertCustomers(customers []model.Customer) error {
	stub.Customers = append(stub.Customers, customers...)
	return nil
}

// InsertCustomersJobsOnQueue ...
func (stub *Stub) InsertCustomersJobsOnQueue(customerJobs []model.CustomerJob) error {
	stub.CustomersJob = append(stub.CustomersJob, customerJobs...)
	return nil
}

// ReadNextCustomersJob ...
func (stub Stub) ReadNextCustomersJob(amount int) ([]model.Customer, error) {
	customers := []model.Customer{}
	counter := 0

	for _, customerJob := range stub.CustomersJob {
		if !customerJob.InProcess {
			// We assume the stub.Customer list is orderered
			customer := stub.Customers[customerJob.CustomerID-1]
			customers = append(customers, customer)
			counter++
			if counter == amount {
				return customers, nil
			}
		}
	}

	fmt.Printf("\n Returning customers len: %d", len(customers))
	return customers, nil
}

// UpdateCustomerJobsInProcess ...
// Set to true the InProcess filed of those CustomersJobs
// whose CustomerID is content in customers
func (stub *Stub) UpdateCustomerJobsInProcess(ids []int, status bool) error {
	for i, customerJob := range stub.CustomersJob {
		if isInList(ids, customerJob.CustomerID) {
			stub.CustomersJob[i].InProcess = status
		}
	}
	return nil
}

// RemoveCustomerJobs ...
func (stub *Stub) RemoveCustomerJobs(ids []int) error {
	for i := 0; i < len(stub.CustomersJob); i++ {
		if isInList(ids, stub.CustomersJob[i].CustomerID) {
			stub.CustomersJob = append(stub.CustomersJob[:i], stub.CustomersJob[i+1:]...)
			i = i - 1
		}
	}
	return nil
}

// CheckStatus ...
func (stub Stub) CheckStatus() {
	fmt.Printf("\nCheck Status")
	fmt.Printf("\n\tCustomers: %+v", stub.Customers)
	fmt.Printf("\n\tCustomersJob: %+v", stub.CustomersJob)
}

func isInList(ids []int, looked int) bool {
	for _, id := range ids {
		if id == looked {
			return true
		}
	}
	return false
}
