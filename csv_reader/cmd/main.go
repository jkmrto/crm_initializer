package main

import (
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/config"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/csv_loader"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/db"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/export"
	"gitlab.com/jkmrto/crm_initializer/csv_reader/internal/publisher"
)

func main() {
	conf := config.LoadAppConfig()

	// Startup db connection
	dbHandler := db.StartPostgresConnection(config.GetPostgresConfig(conf))

	// Load CSV to DB, In a goroutine to not block the exporting process
	go csv_loader.LoadToDB(conf.CustomersCSV, dbHandler)

	// Export customers from DB to CRM Integrator
	publisher := publisher.NewHTTPublisher(config.GetHTTPPublisherConfig(conf))
	export.Export(dbHandler, publisher, make(chan interface{}))
}
