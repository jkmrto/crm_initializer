COVERAGE=$(go test -cover -coverpkg=./... ./... |  grep "coverage" | awk '{print $5}' |  cut -f1 -d'%' | awk '{s+=$1} END {printf "%.1f", s}')
echo "TotalCoverage: ${COVERAGE}%"
