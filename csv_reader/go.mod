module gitlab.com/jkmrto/crm_initializer/csv_reader

go 1.12

require (
	github.com/lib/pq v1.3.0
	github.com/stretchr/testify v1.4.0
	github.com/tkanos/gonfig v0.0.0-20181112185242-896f3d81fadf
	gitlab.com/jkmrto/crm_initializer/crm_integrator v0.0.0-20200224112825-51c2364f28a5 // indirect
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d // indirect
	golang.org/x/net v0.0.0-20200222125558-5a598a2470a0 // indirect
)
