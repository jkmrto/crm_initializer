module gitlab.com/jkmrto/crm_initializer/crm_integrator

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	gitlab.com/jkmrto/crm_initializer/csv_reader v0.0.0-20200128191548-aa65ee19e3bd // indirect
)
