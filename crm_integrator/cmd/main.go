package main

import "gitlab.com/jkmrto/crm_initializer/crm_integrator/internal/api"

func main() {
	api.StartServer()
}
