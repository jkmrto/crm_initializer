package core

import (
	"log"
	"math/rand"
	"os"
	"sort"
	"strconv"

	"gitlab.com/jkmrto/crm_initializer/crm_integrator/internal/model"
)

var successPercentage int32
var eCustomersID []int // expectedCustomersIDs

func init() {
	successPercentage = getSuccessPercentage()
	eCustomersID = makeRange(1, 1000)
}

// ProcessCustomers decides which customers will be successfully saved and which ones will fail
func ProcessCustomers(customers []model.Customer) []model.CustomerJobResult {
	jobResultList := []model.CustomerJobResult{}
	failIds := []int{}
	for _, customer := range customers {
		result := assignResultRandomly()
		jobResult := model.CustomerJobResult{customer.ID, result}
		jobResultList = append(jobResultList, jobResult)
		if result == "success" {
			removeCustomerID(customer.ID)
		} else {
			failIds = append(failIds, customer.ID)
		}
	}

	ids := model.GetIds(customers)
	log.Printf("Received IDs: %d, Fails: %+v, Pending: %d\n", ids, failIds, len(eCustomersID))
	return jobResultList
}

// Get the success pertcentage based on the ENV Variable SUCCESS_PERCENTAGE
func getSuccessPercentage() int32 {
	successPercentageStr := os.Getenv("SUCCESS_PERCENTAGE")
	successPercentage, err := strconv.Atoi(successPercentageStr)
	if err != nil {
		log.Printf("Error geting SUCCESS_PERCENTAGE. It should be set as environment variable SUCCESS_PERCENTAGE, %+v", err)
	}
	return int32(successPercentage)
}

func removeCustomerID(IDtoRemove int) {
	pos := sort.IntSlice(eCustomersID).Search(IDtoRemove)
	eCustomersID = append(eCustomersID[0:pos], eCustomersID[pos+1:len(eCustomersID)]...)
}

func assignResultRandomly() string {
	if rand.Int31n(100) < successPercentage {
		return "success"
	}
	return "fail"
}

func makeRange(min, max int) []int {
	a := make([]int, max-min+1)
	for i := range a {
		a[i] = min + i
	}
	return a
}
