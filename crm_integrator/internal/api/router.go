package api

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

const port = 5010

// StartServer ...
func StartServer() {
	router := mux.NewRouter()
	router.HandleFunc("/crm_integrator/customer_list/new", newCutomerList)
	log.Printf("Waiting for connections on port %d", port)
	http.ListenAndServe(fmt.Sprintf(":%d", port), router)
}
