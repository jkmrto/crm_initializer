package api

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/jkmrto/crm_initializer/crm_integrator/internal/core"
	"gitlab.com/jkmrto/crm_initializer/crm_integrator/internal/model"
)

func newCutomerList(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodPost {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	if req.Header.Get("Content-type") == "application/json" {
		customerList, err := decodeCustomerList(req.Body)

		if err != nil {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(http.StatusUnprocessableEntity)
			io.WriteString(w, fmt.Sprintf("%s+v", err))
		} else {
			responseBody := buildReponseBody(customerList)
			w.Header().Set("Content-Type", "application/json")
			w.WriteHeader(http.StatusOK)
			w.Write(responseBody)
		}
	} else {
		w.WriteHeader(http.StatusUnprocessableEntity)
	}
}

func buildReponseBody(customers []model.Customer) []byte {
	jobsResult := core.ProcessCustomers(customers)
	jobsResultEncoded, _ := json.Marshal(jobsResult)
	return jobsResultEncoded
}

func decodeCustomerList(body io.Reader) ([]model.Customer, error) {
	customers := []model.Customer{}
	decoder := json.NewDecoder(body)
	err := decoder.Decode(&customers)
	return customers, err
}
