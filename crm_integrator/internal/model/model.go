package model

// Customer ...
type Customer struct {
	ID        int    `json:"id"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
}

// CustomerJobResult ...
type CustomerJobResult struct {
	CustomerID int    `json:"customerId"`
	Result     string `json:"result"`
}

// GetIds ...
func GetIds(customers []Customer) []int {
	ids := []int{}
	for _, customer := range customers {
		ids = append(ids, customer.ID)
	}
	return ids
}
